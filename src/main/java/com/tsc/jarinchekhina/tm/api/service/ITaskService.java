package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.api.IService;
import com.tsc.jarinchekhina.tm.entity.Task;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface ITaskService extends IService<Task> {

    void clear(@Nullable String userId);

    @NotNull
    List<Task> findAll(@Nullable String userId);

    @Nullable
    List<Task> findAll(@Nullable String userId, @Nullable Comparator<Task> comparator);

    @Nullable
    Task add(@Nullable String userId, @Nullable Task task);

    @NotNull
    Task create(@Nullable String userId, @Nullable String name);

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Optional<Task> findById(@Nullable String userId, @Nullable String id);

    @NotNull
    Optional<Task> findByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Optional<Task> findByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Optional<Task> remove(@Nullable String userId, @Nullable Task task);

    @NotNull
    Optional<Task> removeById(@Nullable String userId, @Nullable String id);

    @NotNull
    Optional<Task> removeByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Optional<Task> removeByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Task updateTaskById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Task updateTaskByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Task startTaskById(@Nullable String userId, @Nullable String id);

    @NotNull
    Task startTaskByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Task startTaskByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Task finishTaskById(@Nullable String userId, @Nullable String id);

    @NotNull
    Task finishTaskByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Task finishTaskByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Task changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Task changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    Task changeTaskStatusByName(@Nullable String userId, @Nullable String name, @Nullable Status status);

}
