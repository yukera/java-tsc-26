package com.tsc.jarinchekhina.tm.exception.system;

import com.tsc.jarinchekhina.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

public final class HashIncorrectException extends AbstractException {

    public HashIncorrectException() {
        super("Error! Hash util doesn't work...");
    }

}
